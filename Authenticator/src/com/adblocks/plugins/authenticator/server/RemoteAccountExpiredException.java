package com.adblocks.plugins.authenticator.server;

import java.rmi.RemoteException;

public class RemoteAccountExpiredException extends RemoteException{
	
	private static final long serialVersionUID = 6120057922939947773L;
	
	private static final String type = "RemoteAccountExpiredException";

	public RemoteAccountExpiredException() {
		
	}
	
	/**
	 * @param message
	 */
	public RemoteAccountExpiredException(String message) {
		super(message);
	}
	
	/**
	 * @param message
	 * @param cause
	 */
	public RemoteAccountExpiredException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}
	
	public String getExceptionType() {
		return type;
	}
}
