package com.adblocks.plugins.authenticator.server;

import java.rmi.RemoteException;

public class RemoteBadCredentialsException extends RemoteException{
	
	private static final long serialVersionUID = -7261929463445591145L;

	private static final String type = "RemoteBadCredentialsException";
	
	public RemoteBadCredentialsException() {
		
	}
	
	public RemoteBadCredentialsException(String message) {
		super(message);
	}
	
	/**
	 * @param message
	 * @param cause
	 */
	public RemoteBadCredentialsException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}
	
	public String getExceptionType() {
		return type;
	}
}
