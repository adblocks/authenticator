package com.adblocks.plugins.authenticator.server;

import java.rmi.RemoteException;

public class RemoteLockedException extends RemoteException{
	
	private static final long serialVersionUID = 2687857101462318019L;

	private static final String type = "RemoteLockedException";
	
	public RemoteLockedException() {
		
	}
	
	/**
	 * @param message
	 */
	public RemoteLockedException(String message) {
		super(message);
	}
	
	/**
	 * @param message
	 * @param cause
	 */
	public RemoteLockedException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}
	
	public String getExceptionType() {
		return type;
	}
}
