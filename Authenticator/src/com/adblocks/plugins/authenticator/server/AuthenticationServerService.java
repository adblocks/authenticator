package com.adblocks.plugins.authenticator.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

/********************************************
 * 
 * @description	This is the interface for the server-side portion of the Authenticator plugin
 * 
 * @history MSF	08/07/2013	Created
 * 
 ********************************************/

public interface AuthenticationServerService extends Remote
{
	boolean authenticateAD(String domain, String url, String user, String password) throws RemoteException;
	boolean authenticateLDAP(String domain, String url, String serviceaccount, String servicepassword, String user, String password) throws RemoteException;
		
	String getPluginVersion() throws RemoteException;	
}
