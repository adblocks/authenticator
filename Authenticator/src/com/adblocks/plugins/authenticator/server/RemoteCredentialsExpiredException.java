package com.adblocks.plugins.authenticator.server;

import java.rmi.RemoteException;

public class RemoteCredentialsExpiredException extends RemoteException{

	private static final long serialVersionUID = 6120057922939947773L;
	
	private static final String type = "RemoteCredentialsExpiredException";

	public RemoteCredentialsExpiredException() {
		
	}
	
	/*
	 * @param message
	 */
	public RemoteCredentialsExpiredException(String message) {
		super(message);
	}
	
	/**
	 * @param message
	 * @param cause
	 */
	public RemoteCredentialsExpiredException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}
	
	public String getExceptionType() {
		return type;
	}
}
