package com.adblocks.plugins.authenticator.server;

import java.rmi.RemoteException;

public class RemoteDisabledException extends RemoteException{

	private static final long serialVersionUID = 3675190746742309851L;
	
	private static final String type = "RemoteDisabledException";

	public RemoteDisabledException() {
		
	}
	
	/**
	 * @param message
	 */
	public RemoteDisabledException(String message) {
		super(message);
	}
	
	/**
	 * @param message
	 * @param cause
	 */
	public RemoteDisabledException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}
	
	public String getExceptionType() {
		return type;
	}
}
