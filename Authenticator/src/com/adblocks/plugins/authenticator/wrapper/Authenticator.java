package com.adblocks.plugins.authenticator.wrapper;

import java.beans.PropertyChangeEvent;
import java.net.URL;
import java.util.Properties;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import com.servoy.j2db.plugins.IClientPlugin;
import com.servoy.j2db.plugins.IClientPluginAccess;
import com.servoy.j2db.plugins.PluginException;
import com.servoy.j2db.preference.PreferencePanel;
import com.servoy.j2db.scripting.IScriptObject;

public class Authenticator implements IClientPlugin
{
	private IClientPluginAccess application;
	private IScriptObject scriptObject;
	
	
	public Properties getProperties()
	{
		Properties props = new Properties();
		props.put(DISPLAY_NAME, getName());
		return null;
	}

	
	public void load() throws PluginException
	{
		// ignore
	}

	
	public void unload() throws PluginException
	{
		application = null;
		scriptObject = null;
	}

	
	public void propertyChange(PropertyChangeEvent evt)
	{
		// ignore
	}

	public Icon getImage()
	{
		URL iconUrl = getClass().getResource("images/adsailsicon.gif");
		
		if (iconUrl != null)
		{
			return new ImageIcon(iconUrl);
		}
		else
		{
			return null;
		}
	}

	
	public String getName()
	{
		return "Authenticator";
	}


	public PreferencePanel[] getPreferencePanels()
	{
		return null;
	}

	
	public IScriptObject getScriptObject() {
		if (scriptObject == null)
		{
			scriptObject = new AuthenticatorProvider(application);
		}
		return scriptObject;
	}

	
	public void initialize(IClientPluginAccess arg0) throws PluginException {
		application = arg0;
	}

}
