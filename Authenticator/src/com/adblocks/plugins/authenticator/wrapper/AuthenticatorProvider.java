package com.adblocks.plugins.authenticator.wrapper;

import java.rmi.RemoteException;

import com.adblocks.plugins.authenticator.server.AuthenticationServerService;
import com.servoy.j2db.plugins.IClientPluginAccess;
import com.servoy.j2db.scripting.IScriptObject;

public class AuthenticatorProvider implements IScriptObject
{
	private IClientPluginAccess application;
	private AuthenticationServerService authserver;
	
	public AuthenticatorProvider(IClientPluginAccess app)
	{
		super();
		application = app;
	}
	
	// Method gets a connection to the plugin on the server
	private void getServerPluginConnection()
	{
		// Only create it if it hasn't been created yet
		if (authserver == null)
		{
			try
			{
				authserver = (AuthenticationServerService) application.getServerService("com.adblocks.plugins.authenticator.server");
			}
			catch (Exception e)
			{
			
			}
		}
	}
	
	@Override
	public Class<?>[] getAllReturnedTypes() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] getParameterNames(String methodName) {
		String []params = null;
		
		if ( methodName.equals("authenticateAD") )
		{
				params = new String[] {"domain", "serveraddress", "username", "password", "[serverport]"};
		}
		if ( methodName.equals("authenticateLDAP") )
		{
				params = new String[] {"domain", "serveraddress", "serviceuser", "servicepassword", "username", "password", "[serverport]"};
		}
		
		return params;
	}

	@Override
	public String getSample(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getToolTip(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isDeprecated(String arg0) {
		// TODO Auto-generated method stub
		return false;
	}
	
	// PLUGIN METHODS START HERE
	
	public boolean js_authenticateAD(String domain, String serveraddress, String user, String password) throws Exception
	{
		return js_authenticateAD(domain, serveraddress, user, password, 389); //This is the non-encrypted default. 636 is the encrypted port
	}
	
	public boolean js_authenticateAD(String domain, String serveraddress, String user, String password, Integer port) throws Exception
	{
		// Make sure server connection is there
		getServerPluginConnection();
		
		boolean bPassed = false;
		
		// Make the call to the server method
		try
		{
			bPassed = authserver.authenticateAD(domain, "ldap://" + serveraddress + ":" + port.toString(), user, password);
		}
		catch (Exception e)
		{
			throw e;
		}
		return bPassed;
	}
	
	public boolean js_authenticateLDAP(String domain, String serveraddress, String serviceaccount, String servicepassword, String user, String password) throws Exception
	{
		return js_authenticateLDAP(domain, serveraddress, serviceaccount, servicepassword, user, password, 389); //This is the non-encrypted default. 636 is the encrypted port
	}
	
	public boolean js_authenticateLDAP(String domain, String serveraddress, String serviceaccount, String servicepassword, String user, String password, Integer port) throws Exception
	{
		// Make sure server connection is there
		getServerPluginConnection();
		
		boolean bPassed = false;
		
		// Make the call to the server method
		try
		{
			bPassed = authserver.authenticateLDAP(domain, "ldap://" + serveraddress + ":" + port.toString(), serviceaccount, servicepassword, user, password);
		}
		catch (Exception e)
		{
			throw e;
		}
		return bPassed;
	}
	
	
	public String js_getVersion() throws RemoteException
	{
		// Make sure server connection is there
		getServerPluginConnection();
				
		// Make the call to the server method
		try
		{
			return authserver.getPluginVersion();
		}
		catch (RemoteException e)
		{
			throw e;
		}
	}
}
