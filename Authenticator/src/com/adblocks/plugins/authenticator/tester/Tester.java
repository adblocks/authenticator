package com.adblocks.plugins.authenticator.tester;

import java.rmi.RemoteException;
import javax.naming.NamingException;

import com.adblocks.plugins.authenticator.server.AuthenticationServer;


public class Tester
{
	/**
	 * @param args
	 * @throws NamingException 
	 */
	public static void main(String[] args) throws RemoteException
	{
		com.adblocks.plugins.authenticator.server.AuthenticationServer as = new AuthenticationServer();
		
		/** regular authentication test
		try {
			System.out.print(as.authenticateLDAP("adblocks.com", "ldap://10.200.1.2:389", "mfrizzell", "Alt3rnat3g3rbiL"));
		} catch (RemoteException e1) {
			System.out.println(e1.getClass().getName());
			// TODO Auto-generated catch block
			System.out.println(e1.getMessage());
			//e1.printStackTrace();
			throw e1;
		}
		*/
		
		/** LDAP authentication with a service account test*/
		try {
			System.out.print(as.authenticateLDAP("adblocks.com", "ldap://abdata2:389", "bgarfield", "123Abc#", "cn=Administrator,cn=Users", "tumi654"));
		} catch (RemoteException e1) {
			System.out.println(e1.getClass().getName());
			// TODO Auto-generated catch block
			System.out.println(e1.getMessage());
			//e1.printStackTrace();
			throw e1;
		}
	
	/**
		try {
			System.out.print(as.authenticate("adblocks.com", "ldap://10.200.1.2:389", "bgarfield", "bensucks"));
		} catch (RemoteException e1) {
			System.out.println(e1.getClass().getName());
			// TODO Auto-generated catch block
			System.out.println(e1.getMessage());
			//e1.printStackTrace();
			throw e1;
		}
	*/	
	
		/**
		try
		{
			ActiveDirectoryLdapAuthenticationProvider adlap = new ActiveDirectoryLdapAuthenticationProvider("adblocks.com", "ldap://10.200.1.2:389");
			Authentication auth = new UsernamePasswordAuthenticationToken("bgarfield", "bensucks");
			Authentication result = adlap.authenticate(auth);
			result.isAuthenticated();
			
			//Authentication auth = new UsernamePasswordAuthenticationToken("mfrizzell@adblocks.com", "XXXX");
			System.out.print(result.isAuthenticated() + "\n");
			System.out.print(result.getDetails() + "\n");
			System.out.print(result.toString() + "\n");
		}
		catch (Exception e)
		{
			System.out.print(e.getMessage());
		}
		**/
		
		/**
		// Set up the environment for creating the initial context
		Hashtable env = new Hashtable();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		//env.put(Context.PROVIDER_URL, "ldap://" + args[0] + ":" + args[1] + "/"); //10.200.1.2:389");
		env.put(Context.PROVIDER_URL, "ldap://" + "10.200.1.2" + ":" + "389" + "/"); //10.200.1.2:389");
		**/
		
		/**
		// Authenticate as S. User and password "mysecret"
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL, args[2]); //"cn=James A. Garfield,cn=Users,DC=adblocks,dc=com");
		env.put(Context.SECURITY_CREDENTIALS, args[3]); //"XXXX");

		
		// Hard coded authentication
		env.put(Context.PROVIDER_URL, "ldap://10.200.1.2:389");		
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL, "cn=James A. Garfield,cn=Users,DC=adblocks,dc=com");
		env.put(Context.SECURITY_CREDENTIALS, "XXXX");
		 **/
		
		
	
	}
	
}
